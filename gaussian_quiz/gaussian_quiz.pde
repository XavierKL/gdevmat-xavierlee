void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  background(0);
  
}

void draw(){
  
  float gaussian = randomGaussian();
  float mean = 10;
  float standardDeviation = 200;
  float x = (standardDeviation * gaussian) + mean;
  float mean2 = 10;
  float standardDeviation2 = 200;
  float y = (standardDeviation * gaussian) + mean;
  
  
  noStroke();
  fill(random(255), random(255), random(255));
  circle(x, y, x);
  
 
  
}
