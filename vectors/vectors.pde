
Mover mover;

void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

/*float x, y;
float xPeed = 10, yPeed = 15;

PVector position = new PVector();
PVector velocity = new PVector(10, 15);*/

void draw(){

  background(255);
  mover.render();
  
   
   if (mover.position.x > Window.right / Window.left){
     
    mover.acceleration.x -= .01;
    mover.velocity.x -= .2;
    
    if (mover.acceleration.x < 1){
      
      mover.acceleration.x = 0;
    }
    
    if (mover.velocity.x < 1){
     
      mover.velocity.x = 0;
    }
   }
  }
