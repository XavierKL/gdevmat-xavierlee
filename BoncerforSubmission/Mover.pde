public class Mover{
 
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass = 1;
  public float r;
  public float g;
  public float b;
  public float a;
  
  public void render(){
    
    update();
    
    fill(r, g, b, a);
    circle(position.x, position.y, this.mass * 10);
  }
  
  public void update(){
    
    //adds acceleration to velocity in every frame
    this.velocity.add(this.acceleration);
    
    //if you want to limit the speed
    this.velocity.limit(30);
    
    //adds velocity to position every frame
    this.position.add(this.velocity);
    
    //resets acceleration to 0 to avoid over-value
    this.acceleration.mult(0);
  }
  
  public void applyForce(PVector force){ // 2nd law of motion
   
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
  
  public void setColor(float r, float g, float b, float a){
   
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  
   public void applyGravity(){
   
    PVector gravity = new PVector(0, -0.5f * this.mass, 0);
    this.applyForce(gravity);
  }
  
  public void applyFriction(float frictionIntensity){
   
    float c = frictionIntensity;
    float normal = 1;
    float frictionMagnitude = c * normal;
    
    PVector friction = this.velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    this.applyForce(friction);
  }
}
