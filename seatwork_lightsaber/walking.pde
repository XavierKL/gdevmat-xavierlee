class walking{
  
  float xPosition;
  float yPosition;
  float tX;
  float tY; 
  walking(){
   
    tX = 0;
    tY = 10000;
  }

  void render(){ // sourced from https://natureofcode.com/book/introduction/
  
 
   circle(xPosition, yPosition, 30);  
  }
  void walker(){ 
    
    xPosition = map(noise(tX), 0, 1, Window.left, Window.right);
    yPosition = map(noise(tY), 0, 1, Window.bottom, Window.top);
    
    tX += 0.01f;
    tY += 0.01f;
  }

}
  
  
