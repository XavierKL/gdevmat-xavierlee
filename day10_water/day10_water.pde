Mover[] waterBalls;
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);

void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  waterBalls = new Mover[10];
  
  for (int i = 0; i < 10; i++){
   
    waterBalls[i] = new Mover(Window.left + 50 * (i * 2), 400);
    waterBalls[i].mass = i + .05;
    waterBalls[i].setColor(random(255), random(255), random(255), 200);
  }
}

PVector wind = new PVector(0.01f, 0);

void draw(){
  
  background(255);
  
  ocean.render();
  noStroke();
  
  for (Mover wb: waterBalls){
    
    wb.applyGravity();
    wb.applyFriction();
    wb.applyForce(wind);
    
    wb.render();
    wb.update();
    
    if(wb.position.x > Window.right){
      
      wb.velocity.x *= -1;
      wb.position.x = Window.right;
    }
    
    if(wb.position.y < Window.bottom){
      
      wb.velocity.y *= -1;
      wb.position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(wb)){
      
      wb.applyForce(ocean.calculateDragForce(wb));
    }
  }
}  
