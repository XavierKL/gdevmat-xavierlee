void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan (PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  
}

float x, y;
float xPeed = 10, yPeed = 15;

PVector position = new PVector();
PVector velocity = new PVector(10, 15);

walking walking = new walking();

PVector mousePos(){
  
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new PVector(x, y);
}

float t_r = 0;
float t_g = 0;
float t_b = 0;

void draw(){
  
  background(0);
  fill(0, 0, 0);
  
   
  /*position.add(velocity);
  
  
  if (position.x > Window.right || position.x < Window.left){
    
    velocity.x *= -1;
  }
  
  if (position.y > Window.top || position.y < Window.bottom){
    
    velocity.y *= -1;
  }
  
  circle(position.x, position.y, 50);*/
  float perlin1 = noise(t_r);
  float perlin2 = noise(t_g);
  float perlin3 = noise(t_b);
  
  float colors1 = map(perlin1, 0, 1, 10, 255);
  float colors2 = map(perlin2, 0, 1, 50, 200);
  float colors3 = map(perlin2, 0, 1, 50, 150);
  t_r += 0.1f;
  t_g += 0.01f;
  t_b += 0.05f;
  PVector mouse = mousePos();
  mouse.normalize();
  mouse.mult(600);
  strokeWeight(15); // determines thickness of the stroke
  stroke(colors1, colors2, colors3); // colors the stroke
  line(0, 0, mouse.x, mouse.y);
  
  mouse.normalize();
  mouse.mult(600);
  strokeWeight(15);
  stroke(colors1, colors2, colors3);
  line(-mouse.x, -mouse.y, 0, 0);
  
  mouse.normalize();
  mouse.mult(150);
  strokeWeight(15);
  stroke(100);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
  
}
