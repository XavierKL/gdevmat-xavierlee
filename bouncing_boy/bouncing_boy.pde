Mover mover;

void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  
  mover.position.x = Window.left + 50;
 // mover.acceleration = new PVector(0.1 , 0); // if you want to make something move just modify the acceleration. 
  //don't modify position and velocity only the acceleration unless you want object to stop
}

PVector wind = new PVector(.1f, 0);
PVector gravity = new PVector(0, -1);

void draw(){
  
  background(255);
  
  mover.update();
  mover.render();
  
  mover.applyForce(wind);
  mover.applyForce(gravity);
  
  if (mover.position.y < Window.bottom){
    
    mover.velocity.y *= -1; // 3rd law of motion
    mover.position.y = Window.bottom; // safe guard to not exceed
  }
  
  if (mover.position.x > Window.right){
    mover.velocity.x *= -1;
    mover.position.x = Window.right;
  }
}
