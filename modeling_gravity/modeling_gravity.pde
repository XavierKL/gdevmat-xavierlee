Mover[] mover;

void setup(){
 
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover[5];
  

  for (int i = 0; i < 5; i++){
    
    mover[i] = new Mover(Window.left + 50, 50 * (i * 2));
    mover[i].mass = i + random(1 , 5);
    mover[i].acceleration = new PVector(.01f, 0);
    mover[i].setColor(random(255), random(255), random(255), 255);
  }
}
float friction = 0;

void draw(){
 
 background(255);
 noStroke();
 
 for (Mover m : mover){
   
   m.applyFriction(friction);
   
   m.update(); 
   m.render();
  
   if (m.position.x > Window.right / Window.left ){
     
     friction += 0.0001;
     m.velocity.x -= .002;
     
     if (m.velocity.x < 0){
      
       m.velocity.x = 0;
       m.acceleration.x = 0;
     }
     
     if (friction > .05){
       
       friction = 0;
     }
   }
 }
}
