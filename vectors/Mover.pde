public class Mover{
 
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float scale = 50;
  
  public void render(){
    
    update();
    
    fill(0);
    circle(position.x, position.y, scale);
  }
  
  private void update(){
    
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
  }
}
