Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
}

void draw()
{
  background(255);
  
  mover.render();
  mover.update();
  mover.setColor(100, 255, 50, 255);
  
  if (mover.position.x > Window.right)
  {
    mover.position.x = Window.left; 
  }
  

}
