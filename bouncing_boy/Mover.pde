public class Mover{
 
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass = 1;
  public float scale = 50;
  
  public void render(){
    
    update();
    
    fill(0);
    circle(position.x, position.y, scale);
  }
  
  public void update(){
    
    //adds acceleration to velocity in every frame
    this.velocity.add(this.acceleration);
    
    //if you want to limit the speed
    this.velocity.limit(30);
    
    //adds velocity to position every frame
    this.position.add(this.velocity);
    
    //resets acceleration to 0 to avoid over-value
    this.acceleration.mult(0);
  }
  
  public void applyForce(PVector force){ // 2nd law of motion
   
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
}
