void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30 / 180), 0, 0, 0, 0, -1, 0);
  background(255);
}

Walker walker = new Walker();

void draw(){
 
  walker.render();
  walker.randomWalk();
 color c = color(255, 204, 0);
 fill(c);
 noStroke();
}
