public class Mover{
 
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float mass = 1;
  public float r;
  public float g;
  public float b;
  public float a;
  
  Mover(){
    
  }
  
  Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
   }
   
   Mover(PVector position)
   {
      this.position = position;
   }
   
  public void render(){
    
    update();
    
    fill(r, g, b, a);
    circle(position.x, position.y, this.mass * 15);
  }
  
  public void update(){
    
    //adds acceleration to velocity in every frame
    this.velocity.add(this.acceleration);
    
    //if you want to limit the speed
    //this.velocity.limit(30);
    
    //adds velocity to position every frame
    this.position.add(this.velocity);
  }
  
  public void applyForce(PVector force){ // 2nd law of motion
   
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
  }
  
  public void applyGravity(){
   
    //cancel out the mass
    PVector gravity = new PVector(0, -0.5f * this.mass, 0);
    this.applyForce(gravity);
  }
  
  public void applyFriction(float frictionIntensity){
   
    float c = frictionIntensity; // coefficient of friction
    float normal = 1; // normal force
    float frictionMagnitude = c * normal;
    
    PVector friction = this.velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    this.applyForce(friction);
  }
  
  public void setColor(float r, float g, float b, float a){
    
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
}
