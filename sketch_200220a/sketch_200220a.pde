void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  background(0);
  
}

void draw(){
 
  float gaussian = randomGaussian();
  float mean = 0;
  float standardDeviation = 100;
  float x = (standardDeviation * gaussian) + mean;
  float y = random(height);
  
  noStroke();
  fill(random(255), random(255), random(255));
  circle(x, y, x);
}
