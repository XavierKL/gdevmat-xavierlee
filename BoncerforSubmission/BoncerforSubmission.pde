Mover mover;
Mover[] balls;

void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, -1, 0);
  
  balls = new Mover[10]; // sourced from Vince Velasco
  
  for (int i = 0; i < 10; i++){ // also from vince
    
    balls[i] = new Mover();
    balls[i].position.x = Window.left + 50;
    balls[i].mass = i + 1;
    balls[i].setColor(random(255), random(255), random(255), random(100, 255));
  }
  mover = new Mover();
  mover.mass = 1;
  
  
  mover.position.x = Window.left + 50;
 // mover.acceleration = new PVector(0.1 , 0); // if you want to make something move just modify the acceleration. 
  //don't modify position and velocity only the acceleration unless you want object to stop
}

PVector wind = new PVector(.1f, 0);
PVector gravity = new PVector(0, -1);



void draw(){
  
  background(255);
  
  mover.update();
  mover.render();
  
  mover.applyForce(wind);
  mover.applyForce(gravity); 
  
  for (Mover b : balls){ 
    
   b.render();
   b.update();
   
   b.applyForce(wind);
   b.applyForce(gravity);
    
     
  if (b.position.y < Window.bottom){
    
    b.velocity.y *= -1; // 3rd law of motion
    b.position.y = Window.bottom; // safe guard to not exceed
  }
  
  if (b.position.x > Window.right ){
    
    b.velocity.x *= -1;
    b.position.x = Window.right;
  }
 }
  
  if (mover.position.y < Window.bottom){
    
    mover.velocity.y *= -1; // 3rd law of motion
    mover.position.y = Window.bottom; // safe guard to not exceed
  }
  
  if (mover.position.x > Window.right || mover.position.x < Window.left){
    
    mover.velocity.x *= -1;
    //mover.position.x = Window.right;
  }
 
}
