int numCircles = 100;
circles[] particleEffects =  new circles[numCircles]; // sourced code from https://forum.processing.org/one/topic/how-do-i-make-an-array-of-circles.html

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  for (int i = 0; i < numCircles; i++){
     
     particleEffects[i] = new circles(random(Window.left, Window.right), random(Window.bottom, Window.top));
   }
}

Mover mover = new Mover();
circles particles = new circles();
int frame = 0;

void draw()
{
  background(0);
  frame++;
  if (frame < 60){
    frame = 0; 
    for (int i = 0; i < numCircles; i++){
     
      particleEffects[i].render();
      
    }
     mover.render();
     PVector blackHoleDirection = PVector.sub(mover.position, particles.position);
     blackHoleDirection.normalize();
     particles.position.add(blackHoleDirection);
  }
}
