void setup(){
  
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0f) / tan(PI * 30.0f / 180.0f), 0, 0, 0, 0, - 1, 0);
  background(0);
}

void draw(){
  
  float gaussian = randomGaussian();
  float mean = 0;
  float standardDeviation = 120;
  float x = (standardDeviation * gaussian) + mean;
  
  noStroke();
  fill(255, 10);
  circle(x
  , 0, 60);
}
