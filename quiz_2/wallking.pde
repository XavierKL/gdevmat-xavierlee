class walking{
  
  float xPosition;
  float yPosition;
  float tX;
  float tY; 
  walking(){
   
    tX = 0;
    tY = 10000;
  }

float r_t = 150;
float g_t = 200;
float b_t = 250;
float rM; // map for red  sourced from: James Cabatu
float gM;
float bM;

  void render(){ // sourced from https://natureofcode.com/book/introduction/
  
   float t = 0;
   float perlin = noise(t);
   float mappedValue = map(perlin , 0, 1, 0, Window.top);
   
   float r = noise(r_t);
   rM = map(r, 0, 1, 0, 255);
   
   float g = noise(g_t);
   gM = map(g, 0, 1, 0, 255);
   
   float b = noise(b_t);
   bM = map(b, 0, 1, 0, 255);
   
   r_t += 0.1f;
   g_t += 0.1f;
   b_t += 0.1f;
   t += 0.1f;
   
   fill (rM, gM, bM, 255);
   noStroke();
   circle(xPosition, yPosition, mappedValue);  
  }
  void walker(){ 
    
    xPosition = map(noise(tX), 0, 1, Window.left, Window.right);
    yPosition = map(noise(tY), 0, 1, Window.bottom, Window.top);
    
    tX += 0.01f;
    tY += 0.01f;
  }

}
  
  
