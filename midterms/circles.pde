public class circles{
  
  float x;
  float y;
  float scale = random(50);
  public PVector position;
  
  circles(){
    
    position = new PVector();
  }
  ;  circles(float x, float y){
    
      position = new PVector(x, y);
   }
   
  /*
circles(float x, float y){
   
    this.x = x;
    this.y = y;
}*/
 
 float r_t = 0;
 float g_t = 0;
 float b_t = 0;
 
 void render(){
   float distanceGaussian = randomGaussian();
   float standardDeviation = 100;
   float horizontal = random(Window.left, Window.right); 
   float vertical = random(Window.bottom, Window.top); 
   float x = standardDeviation * distanceGaussian + horizontal;
   float y = standardDeviation * distanceGaussian + vertical;
   
   float perlinR = noise(r_t);
   float perlinG = noise(g_t);
   float perlinB = noise(b_t);
   
   float R = map(perlinR, 0, 1, 0, 255);
   float G = map(perlinG, 0, 1, 0, 255);
   float B = map(perlinB, 0, 1, 0, 255);
   r_t = 0.1f;
   g_t = 0.2f;
   b_t = 0.3f;
   
   fill(R, G, B, 255);
   circle(x , y, scale);
  }
}
